package Test;

import Base.Base;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;


import static io.restassured.RestAssured.given;

public class POST_ProductionResult_Test extends Base {
    String token =doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();

    @BeforeTest()
    public void BeforeMethod(){

        map.put("resultName","Test Result for Deletion2");
        map.put("isOK",true);
        map.put("isActive", true);

        System.out.println(map);
    }

    @Test (priority = 1,description = "200 Success")
    public void POST_Create_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                post(PRODUCTION_PAGE_URL + "ProductionResultType/Create").
                then().
                statusCode(200).log().all();

    }

    @Test (priority = 2, description = "400 Not Success (Empty Result)")
    public void POST_Create_Not_Success() throws InterruptedException {
        map.put("resultName",null);
        map.put("isOK",true);
        map.put("isActive", true);
        System.out.println(map);

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                body(map).
                when().
                post(PRODUCTION_PAGE_URL + "ProductionResultType/Create").
                then().
                statusCode(400).log().all();
    }
}

