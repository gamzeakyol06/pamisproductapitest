package Test;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import Base.Base;
import static io.restassured.RestAssured.given;

public class PUT_ProductionTag_Test extends Base {

    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();
    @BeforeTest()
    public void BeforeMethod(){
        map.put("id",61);
        map.put("productionResultTypeID",2);
        map.put("tagID",1502);
        map.put("deviceID", 10);
        map.put("isActive", true);

        System.out.println(map);
    }

    @Test(priority = 1,description = "200 Success")
    public void PUT_Update_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                put(PRODUCTION_PAGE_URL + "ProductionTag/Update?id="+61).
                then().
                statusCode(204).log().all();
    }

    @Test (priority = 2)
    public void PUT_Update_Assert_Test() throws InterruptedException, IOException {

        Response response = doGetRequest(PRODUCTION_PAGE_URL+"ProductionTag");
        List<Integer> jsonResponse_listid = doGetResponseListID(response);
        List<Integer> jsonResponse_tagId = doGetResponseListtagID(response);

        for (int i = 0; i < jsonResponse_listid.size(); i++) {
            Integer postNameID = jsonResponse_listid.get(i);
            System.out.println(postNameID);
            if(postNameID == 61) {
                Integer posttagID = jsonResponse_tagId.get(i);
                System.out.println(posttagID);
                System.out.println("Result tag ID " + posttagID);

                Assert.assertEquals(posttagID, map.get("tagID"));
                System.out.println(posttagID);
                System.out.println(map.get("tagID"));
            }
        }

    }

}
